import { Injectable } from '@angular/core';
import { StorageService } from '../storage/storage.service';
import { Observable, Subscription, from } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { BLE } from '@ionic-native/ble/ngx';
import { SupportService } from '../services/support.service';
import { resolve } from 'url';

// Bluetooth UUIDs
const UUID_SERVICE = 'FFF0';
const WRITE_CHARACTERISTIC = 'FFF2';
const READ_CHARACTERISTIC = 'FFF1';

@Injectable()
export class BleService {

  peripheral: any = {};
  private connection: Subscription;
  private connectionCommunication: Subscription;
  private reader: Observable<any>;

  constructor(
    private ble: BLE,
    private storage: StorageService,
    private support: SupportService
  ) {  }

  checkConnection(id: string) {
    return new Promise((resolve, reject) => {
      this.ble.isConnected(id).then(isConnected => {
        resolve('블루투스 연결됨');
      }, notConnected => {
        reject('블루투스 연결안됨');
      });
    });
  }

  deviceConnection(id: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.connection = this.ble.connect(id).subscribe(
        peripheral => {
          this.peripheral = peripheral;
          this.storage.setBluetoothId(id);
          resolve('블루투스 연결됨');
        }, 
        fail => {
          console.log(`[bluetooth.service-88] Error conexión: ${JSON.stringify(fail)}`);
          reject('블루투스를 연결할수 없습니다.');
      });
    });
  }

  disconnect(): Promise<boolean> {
    return new Promise((result) => {
      if (this.connectionCommunication) {
        this.connectionCommunication.unsubscribe();
      }
      if (this.connection) {
        this.connection.unsubscribe();
      }
      this.ble.disconnect(this.peripheral.id);
      result(true);
    });
  }

  storedConnection(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.getBluetoothId().then(bluetoothId => {
        console.log(`[bluetooth.service-129] ${bluetoothId}`);
        this.deviceConnection(bluetoothId).then(success => {
          resolve(success);
        }, fail => {
          reject(fail);
        });
      });
    });
  }

  printData(data: Uint8Array) {
    return this.checkConnection(this.peripheral.id).then((isConnected) => {
      this.ble.writeWithoutResponse(
        this.peripheral.id, 
        UUID_SERVICE, 
        WRITE_CHARACTERISTIC, 
        data.buffer
      );
    }, notConected => {
        console.log('BLUETOOTH>NOT_CONNECTED');
    });
  }

  printForm(data: Uint8Array) {
    
    const sliced1 = data.slice(0, 200),
          sliced2 = data.slice(200, 400),
          sliced3 = data.slice(400, data.byteLength);

    return this.ble.requestMtu(this.peripheral.id, 512).then(mtu => {
      
      this.ble.writeWithoutResponse(
        this.peripheral.id,
        UUID_SERVICE,
        WRITE_CHARACTERISTIC,
        sliced1.buffer
      ).then( _ => {
        this.ble.writeWithoutResponse(
          this.peripheral.id,
          UUID_SERVICE,
          WRITE_CHARACTERISTIC,
          sliced2.buffer
        ).then(_ => {
          this.ble.writeWithoutResponse(
            this.peripheral.id,
            UUID_SERVICE,
            WRITE_CHARACTERISTIC,
            sliced3.buffer
          )
        })
      })

    });
  }

  dataInOutRealtime():Promise<any> {
    let inputdata = new Uint8Array(3);
    inputdata[0] = 0x02;
    inputdata[1] = 0x20;
    inputdata[2] = 0x03;
    return new Promise((resolve, reject) => {
      this.ble.writeWithoutResponse(this.peripheral.id, UUID_SERVICE, WRITE_CHARACTERISTIC, inputdata.buffer)
      .then(
        data => {
          this.ble
          .startNotification(this.peripheral.id, UUID_SERVICE, READ_CHARACTERISTIC).subscribe(
            buffer => {
              let data = this.bytesToString(buffer[0]);
              resolve(data);
            },
            e => console.error(e)
          );
        },
        err => {
          console.error(err);
        });
    });
  }

  bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
  }

}
