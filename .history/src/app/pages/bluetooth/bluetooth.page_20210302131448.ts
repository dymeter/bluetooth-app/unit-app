import { BLE } from '@ionic-native/ble/ngx';
import { Component, NgZone, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { SupportService } from 'src/app/providers/services/support.service';

@Component({
  selector: 'app-bluetooth',
  templateUrl: 'bluetooth.page.html',
  styleUrls: ['bluetooth.page.scss']
})
export class BluetoothPage implements OnInit {

  devices: any[] = [];
  statusMessage: string;

  constructor(public router: Router, 
              private toastCtrl: ToastController,
              private support: SupportService,
              private ble: BLE,
              private ngZone: NgZone) { 
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter');
    //this.scan();
  }

  ngOnInit() {}

  scan() {
    this.setStatus('Scanning for Bluetooth LE Devices');
    this.devices = [];  // clear list

    this.ble.scan([], 5).subscribe(
      device => this.onDeviceDiscovered(device), 
      error => this.scanError(error)
    );

    setTimeout(this.setStatus.bind(this), 5000, 'Scan complete');
  }

  onDeviceDiscovered(device) {
    console.log('Discovered ' + JSON.stringify(device, null, 2));
    this.ngZone.run(() => {
      this.devices.push(device);
    });
  }

  async scanError(error) {
    this.setStatus('Error ' + error);
    const toast = this.toastCtrl.create({
      message: 'Error scanning for Bluetooth low energy devices',
      position: 'middle',
      duration: 5000
    });
  }

  setStatus(message) {
    this.ngZone.run(() => {
      this.statusMessage = message;
    });
  }

  deviceSelected(device) {
    //this.support.showAlert(`${device.id}_sidfoisdfsjdfklsdfl`);
    //this.support.showAlert(typeof(device.id));
    this.router.navigate( [`/app/connect/${device.id}`]);
  }
  
}
