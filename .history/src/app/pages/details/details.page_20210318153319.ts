import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionSheetController, AlertController, LoadingController, IonContent, Platform } from '@ionic/angular';
import { GetApiService } from 'src/app/providers/services/get-api.service';
import { Storage } from '@ionic/storage';
import { environment } from 'src/environments/environment.prod';
import { BleService, StorageService } from 'src/app/providers/providers';
import { SupportService } from 'src/app/providers/services/support.service';
import { PostApiService } from 'src/app/providers/services/post-api.service';
import EscPosEncoder from 'esc-pos-encoder-ionic';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';

import { finalize } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { BLE } from '@ionic-native/ble/ngx';

const STORAGE_KEY = 'my_images';
@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})

export class DetailsPage implements OnDestroy {
  @ViewChild('content', { static: false }) content: IonContent;

  printerUUID: string;
  DEVICEID = environment.device_id;

  isConnected = false;

  resultId : string;
  results: any;
  
  timestamp: string = "";
  isChecked: boolean;
  DS: number = 0;

  lang: string = '';

  images = [];
  displayedImage: string = "";

  peripheral: any = {};

  constructor(
    private activatedRoute: ActivatedRoute,
    public actionSheetController: ActionSheetController,
    public getapi: GetApiService,
    private storage: Storage,
    private support: SupportService,
    public alertCtrl: AlertController,
    private postapi: PostApiService,
    public loadingController: LoadingController,
    private translate: TranslateService,
    private storageService: StorageService,
    private location: Location,
    private camera: Camera, private file: File, private webview: WebView, private platform: Platform, private ref: ChangeDetectorRef, private filePath: FilePath, private http: HttpClient,
    private ble: BLE,
  ) {
    this.resultId = this.activatedRoute.snapshot.paramMap.get('id');

  }

  ionViewDidEnter(){
    this.scan();
  }

  ionViewWillEnter(){
    this.storageService.getLang().then(lang => {
      if (!lang) {
        const localLang = this.translate.getBrowserLang();
        if(localLang != 'ko') {
          this.lang = 'en';
        } else {
        this.lang = this.translate.getBrowserLang();
        }
      } else {
        this.lang = lang;
      }
    });

    this.storage.get(this.DEVICEID).then(device => {
      this.getapi.bluetoothUUID(device).subscribe((res) => {
        if (this.platform.is('android')) {
          this.printerUUID = res[0].printer_uuid;
        } else if(this.platform.is('ios')){
          this.printerUUID = res[0].printer_ble;
        }
      });
    });

    this.getapi.measureResult(this.resultId).subscribe((res) => {
      this.results = res;
      this.isChecked = this.results[0].checked;
      this.timestamp = `${(this.results[0].datetime).split(/[T.]+/)[0]} ${(this.results[0].datetime).split(/[T.]+/)[1]}`;
      this.countData();
      this.displayedImage = this.results[0].imgurl;
    },
    (err) => {this.support.presentToast(err)});
    
  }

  scan() {
    this.ble.scan([], 5).subscribe(
      device => this.onDeviceDiscovered(device), 
      error => this.support.presentToast('블루투스 스캔 오류')
    );
  }

  onDeviceDiscovered(device) {
    console.log('Discovered ' + JSON.stringify(device, null, 2));
  }

  scrollToBottom(): void {
    this.content.scrollToTop(300);
  }

  checkboxClick(e) {
    this.postapi.updateCheck(this.resultId, {bool: this.isChecked}).subscribe((res: any) => {
      this.countData();
    });
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: this.translate.instant('MORE'),
      buttons: [{
        text: this.translate.instant('PRINT'),
        icon: 'print',
        handler: () => {
          this.print();
        }
      },{
        text: this.translate.instant('DELETE'),
        icon: 'trash',
        handler: () => {
          this.delete();
        },
      },{
        text: this.translate.instant('CLOSE'),
        icon: 'close',
        role: 'cancel',
      }]
    });
    await actionSheet.present();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Printing...',
      duration: 2000
    });
    await loading.present();
  }

  print() {
    const encoder = new EscPosEncoder();
    const result = encoder.initialize();
    if (this.isChecked) {
      if(this.lang == 'ko') {
        result
        .codepage('cp949')
        .korean()
        .start()
        .raw([(this.DS >> 8), (this.DS & 0xff)])
        .line(`${this.results[0].cname}`)  //사업장
        .line(`${this.results[0].ct_name}`)  //계약자
        .line(`${this.results[0].sname}`)  //현장명
        .line(`${this.results[0].memo}`) //메모
        .line(`${(this.results[0].datetime).split(/[T.]+/)[0]} ${(this.results[0].datetime).split(/[T.]+/)[1]}`)  //날짜
        .line(`${this.results[0].mixnum}`) //배합번호
        .line(NumberStr((this.results[0].w1_unit).toString(), 6) + NumberStr((this.results[0].w1_density).toString(), 7))
        .line(NumberStr((this.results[0].w2_unit).toString(), 6) + NumberStr((this.results[0].w2_density).toString(), 7))
        .line(NumberStr((this.results[0].w3_unit).toString(), 6) + NumberStr((this.results[0].w3_density).toString(), 7))
        .line(NumberStr((this.results[0].c1_unit).toString(), 6) + NumberStr((this.results[0].c1_density).toString(), 7))
        .line(NumberStr((this.results[0].c2_unit).toString(), 6) + NumberStr((this.results[0].c2_density).toString(), 7))
        .line(NumberStr((this.results[0].c3_unit).toString(), 6) + NumberStr((this.results[0].c3_density).toString(), 7))
        .line(NumberStr((this.results[0].mad1_unit).toString(), 6) + NumberStr((this.results[0].mad1_density).toString(), 7))
        .line(NumberStr((this.results[0].mad2_unit).toString(), 6) + NumberStr((this.results[0].mad2_density).toString(), 7))
        .line(NumberStr((this.results[0].mad3_unit).toString(), 6) + NumberStr((this.results[0].mad3_density).toString(), 7))
        .line(NumberStr((this.results[0].s1_unit).toString(), 6) + NumberStr((this.results[0].s1_density).toString(), 7))
        .line(NumberStr((this.results[0].s2_unit).toString(), 6) + NumberStr((this.results[0].s2_density).toString(), 7))
        .line(NumberStr((this.results[0].s3_unit).toString(), 6) + NumberStr((this.results[0].s3_density).toString(), 7))
        .line(NumberStr((this.results[0].g1_unit).toString(), 6) + NumberStr((this.results[0].g1_density).toString(), 7))
        .line(NumberStr((this.results[0].g2_unit).toString(), 6) + NumberStr((this.results[0].g2_density).toString(), 7))
        .line(NumberStr((this.results[0].g3_unit).toString(), 6) + NumberStr((this.results[0].g3_density).toString(), 7))
        .line(NumberStr((this.results[0].ad1_unit).toString(), 6) + NumberStr((this.results[0].ad1_density).toString(), 7))
        .line(NumberStr((this.results[0].ad2_unit).toString(), 6) + NumberStr((this.results[0].ad2_density).toString(), 7))
        .line(NumberStr((this.results[0].ad3_unit).toString(), 6) + NumberStr((this.results[0].ad3_density).toString(), 7))
        .line(NumberStr((this.results[0].air).toString(), 6)) // 목표공기량
        .line(NumberStr((this.results[0].aggregate).toString(), 6)) // 골재수정계수
        .line(NumberStr((this.results[0].wet).toString(), 6)) // 시멘트 습윤밀도
        .line(NumberStr((this.results[0].common_mass).toString(), 7)) // 용기질량
        .line(NumberStr((this.results[0].common_water).toString(), 7)) // 용기+물
        .line(NumberStr((this.results[0].common_volume).toString(), 7)) // 용기용적
        .line(NumberStr((this.results[0].input_slump).toString(), 7)) // 슬럼프
        .line(NumberStr((this.results[0].input_temp).toString(), 7)) // 온도
        .line(NumberStr((this.results[0].input_before).toString(), 7)) // 주수전질량
        .line(NumberStr((this.results[0].input_after).toString(), 7)) // 주수후질량
        .line(NumberStr((this.results[0].input_i_pressure).toString(), 7)) // 초기압력
        .line(NumberStr((this.results[0].input_e_pressure).toString(), 7)) // 평형압력
        .line(NumberStr(((this.results[0].mix_volume / 1000).toFixed(3)).toString(), 7)) // 이론용적
        .line(NumberStr((this.results[0].result_mass).toString(), 7)) // 단위용적질량
        .line(NumberStr((this.results[0].result_air).toString(), 7)) // 공기량
        .line(NumberStr((this.results[0].result_quantity).toString(), 7)) // 단위수량
        .end()
      } else if (this.lang == 'en') {
        result
        .codepage('cp949')
        .english()
        .start()
        .raw([(this.DS >> 8), (this.DS & 0xff)])
        .line(`${this.results[0].cname}`)  //사업장
        .line(`${this.results[0].ct_name}`)  //계약자
        .line(`${this.results[0].sname}`)  //현장명
        .line(`${this.results[0].memo}`) //메모
        .line(`${(this.results[0].datetime).split(/[T.]+/)[0]} ${(this.results[0].datetime).split(/[T.]+/)[1]}`)  //날짜
        .line(`${this.results[0].mixnum}`) //배합번호
        .line(NumberStr((this.results[0].w1_unit).toString(), 6) + NumberStr((this.results[0].w1_density).toString(), 7))
        .line(NumberStr((this.results[0].w2_unit).toString(), 6) + NumberStr((this.results[0].w2_density).toString(), 7))
        .line(NumberStr((this.results[0].w3_unit).toString(), 6) + NumberStr((this.results[0].w3_density).toString(), 7))
        .line(NumberStr((this.results[0].c1_unit).toString(), 6) + NumberStr((this.results[0].c1_density).toString(), 7))
        .line(NumberStr((this.results[0].c2_unit).toString(), 6) + NumberStr((this.results[0].c2_density).toString(), 7))
        .line(NumberStr((this.results[0].c3_unit).toString(), 6) + NumberStr((this.results[0].c3_density).toString(), 7))
        .line(NumberStr((this.results[0].mad1_unit).toString(), 6) + NumberStr((this.results[0].mad1_density).toString(), 7))
        .line(NumberStr((this.results[0].mad2_unit).toString(), 6) + NumberStr((this.results[0].mad2_density).toString(), 7))
        .line(NumberStr((this.results[0].mad3_unit).toString(), 6) + NumberStr((this.results[0].mad3_density).toString(), 7))
        .line(NumberStr((this.results[0].s1_unit).toString(), 6) + NumberStr((this.results[0].s1_density).toString(), 7))
        .line(NumberStr((this.results[0].s2_unit).toString(), 6) + NumberStr((this.results[0].s2_density).toString(), 7))
        .line(NumberStr((this.results[0].s3_unit).toString(), 6) + NumberStr((this.results[0].s3_density).toString(), 7))
        .line(NumberStr((this.results[0].g1_unit).toString(), 6) + NumberStr((this.results[0].g1_density).toString(), 7))
        .line(NumberStr((this.results[0].g2_unit).toString(), 6) + NumberStr((this.results[0].g2_density).toString(), 7))
        .line(NumberStr((this.results[0].g3_unit).toString(), 6) + NumberStr((this.results[0].g3_density).toString(), 7))
        .line(NumberStr((this.results[0].ad1_unit).toString(), 6) + NumberStr((this.results[0].ad1_density).toString(), 7))
        .line(NumberStr((this.results[0].ad2_unit).toString(), 6) + NumberStr((this.results[0].ad2_density).toString(), 7))
        .line(NumberStr((this.results[0].ad3_unit).toString(), 6) + NumberStr((this.results[0].ad3_density).toString(), 7))
        .line(NumberStr((this.results[0].air).toString(), 6)) // 목표공기량
        .line(NumberStr((this.results[0].aggregate).toString(), 6)) // 골재수정계수
        .line(NumberStr((this.results[0].wet).toString(), 6)) // 시멘트 습윤밀도
        .line(NumberStr((this.results[0].common_mass).toString(), 7)) // 용기질량
        .line(NumberStr((this.results[0].common_water).toString(), 7)) // 용기+물
        .line(NumberStr((this.results[0].common_volume).toString(), 7)) // 용기용적
        .line(NumberStr((this.results[0].input_slump).toString(), 7)) // 슬럼프
        .line(NumberStr((this.results[0].input_temp).toString(), 7)) // 온도
        .line(NumberStr((this.results[0].input_before).toString(), 7)) // 주수전질량
        .line(NumberStr((this.results[0].input_after).toString(), 7)) // 주수후질량
        .line(NumberStr((this.results[0].input_i_pressure).toString(), 7)) // 초기압력
        .line(NumberStr((this.results[0].input_e_pressure).toString(), 7)) // 평형압력
        .line(NumberStr(((this.results[0].mix_volume / 1000).toFixed(3)).toString(), 7)) // 이론용적
        .line(NumberStr((this.results[0].result_mass).toString(), 7)) // 단위용적질량
        .line(NumberStr((this.results[0].result_air).toString(), 7)) // 공기량
        .line(NumberStr((this.results[0].result_quantity).toString(), 7)) // 단위수량
        .end()
      }
    } else {
      if(this.lang == 'ko') {
        result
        .codepage('cp949')
        .korean()
        .start()
        .raw([(this.DS >> 8), (this.DS & 0xff)])
        .line(`${this.results[0].cname}`)  //사업장
        .line(`${this.results[0].ct_name}`)  //계약자
        .line(`${this.results[0].sname}`)  //현장명
        .line(`${this.results[0].memo}`) //메모
        .line(`${(this.results[0].datetime).split(/[T.]+/)[0]}`) // 날짜
        .line(`${this.results[0].mixnum}`) //배합번호
        .line(NumberStr((this.results[0].w1_unit).toString(), 6) + NumberStr((this.results[0].w1_density).toString(), 7))
        .line(NumberStr((this.results[0].w2_unit).toString(), 6) + NumberStr((this.results[0].w2_density).toString(), 7))
        .line(NumberStr((this.results[0].w3_unit).toString(), 6) + NumberStr((this.results[0].w3_density).toString(), 7))
        .line(NumberStr((this.results[0].c1_unit).toString(), 6) + NumberStr((this.results[0].c1_density).toString(), 7))
        .line(NumberStr((this.results[0].c2_unit).toString(), 6) + NumberStr((this.results[0].c2_density).toString(), 7))
        .line(NumberStr((this.results[0].c3_unit).toString(), 6) + NumberStr((this.results[0].c3_density).toString(), 7))
        .line(NumberStr((this.results[0].mad1_unit).toString(), 6) + NumberStr((this.results[0].mad1_density).toString(), 7))
        .line(NumberStr((this.results[0].mad2_unit).toString(), 6) + NumberStr((this.results[0].mad2_density).toString(), 7))
        .line(NumberStr((this.results[0].mad3_unit).toString(), 6) + NumberStr((this.results[0].mad3_density).toString(), 7))
        .line(NumberStr((this.results[0].s1_unit).toString(), 6) + NumberStr((this.results[0].s1_density).toString(), 7))
        .line(NumberStr((this.results[0].s2_unit).toString(), 6) + NumberStr((this.results[0].s2_density).toString(), 7))
        .line(NumberStr((this.results[0].s3_unit).toString(), 6) + NumberStr((this.results[0].s3_density).toString(), 7))
        .line(NumberStr((this.results[0].g1_unit).toString(), 6) + NumberStr((this.results[0].g1_density).toString(), 7))
        .line(NumberStr((this.results[0].g2_unit).toString(), 6) + NumberStr((this.results[0].g2_density).toString(), 7))
        .line(NumberStr((this.results[0].g3_unit).toString(), 6) + NumberStr((this.results[0].g3_density).toString(), 7))
        .line(NumberStr((this.results[0].ad1_unit).toString(), 6) + NumberStr((this.results[0].ad1_density).toString(), 7))
        .line(NumberStr((this.results[0].ad2_unit).toString(), 6) + NumberStr((this.results[0].ad2_density).toString(), 7))
        .line(NumberStr((this.results[0].ad3_unit).toString(), 6) + NumberStr((this.results[0].ad3_density).toString(), 7))
        .line(NumberStr((this.results[0].air).toString(), 6)) // 목표공기량
        .line(NumberStr((this.results[0].aggregate).toString(), 6)) // 골재수정계수
        .line(NumberStr((this.results[0].wet).toString(), 6)) // 시멘트 습윤밀도
        .line(NumberStr((this.results[0].common_mass).toString(), 7)) // 용기질량
        .line(NumberStr((this.results[0].common_water).toString(), 7)) // 용기+물
        .line(NumberStr((this.results[0].common_volume).toString(), 7)) // 용기용적
        .line(NumberStr((this.results[0].input_slump).toString(), 7)) // 슬럼프
        .line(NumberStr((this.results[0].input_temp).toString(), 7)) // 온도
        .line(NumberStr((this.results[0].input_before).toString(), 7)) // 주수전질량
        .line(NumberStr((this.results[0].input_after).toString(), 7)) // 주수후질량
        .line(NumberStr((this.results[0].input_i_pressure).toString(), 7)) // 초기압력
        .line(NumberStr((this.results[0].input_e_pressure).toString(), 7)) // 평형압력
        .line(NumberStr(((this.results[0].mix_volume / 1000).toFixed(3)).toString(), 7)) // 이론용적
        .line(NumberStr((this.results[0].result_mass).toString(), 7)) // 단위용적질량
        .line(NumberStr((this.results[0].result_air).toString(), 7)) // 공기량
        .line(NumberStr((this.results[0].result_quantity).toString(), 7)) // 단위수량
        .end()
      } else if (this.lang == 'en') {
        result
        .codepage('cp949')
        .english()
        .start()
        .raw([(this.DS >> 8), (this.DS & 0xff)])
        .line(`${this.results[0].cname}`)  //사업장
        .line(`${this.results[0].ct_name}`)  //계약자
        .line(`${this.results[0].sname}`)  //현장명
        .line(`${this.results[0].memo}`) //메모
        .line(`${(this.results[0].datetime).split(/[T.]+/)[0]}`) // 날짜
        .line(`${this.results[0].mixnum}`) //배합번호
        .line(NumberStr((this.results[0].w1_unit).toString(), 6) + NumberStr((this.results[0].w1_density).toString(), 7))
        .line(NumberStr((this.results[0].w2_unit).toString(), 6) + NumberStr((this.results[0].w2_density).toString(), 7))
        .line(NumberStr((this.results[0].w3_unit).toString(), 6) + NumberStr((this.results[0].w3_density).toString(), 7))
        .line(NumberStr((this.results[0].c1_unit).toString(), 6) + NumberStr((this.results[0].c1_density).toString(), 7))
        .line(NumberStr((this.results[0].c2_unit).toString(), 6) + NumberStr((this.results[0].c2_density).toString(), 7))
        .line(NumberStr((this.results[0].c3_unit).toString(), 6) + NumberStr((this.results[0].c3_density).toString(), 7))
        .line(NumberStr((this.results[0].mad1_unit).toString(), 6) + NumberStr((this.results[0].mad1_density).toString(), 7))
        .line(NumberStr((this.results[0].mad2_unit).toString(), 6) + NumberStr((this.results[0].mad2_density).toString(), 7))
        .line(NumberStr((this.results[0].mad3_unit).toString(), 6) + NumberStr((this.results[0].mad3_density).toString(), 7))
        .line(NumberStr((this.results[0].s1_unit).toString(), 6) + NumberStr((this.results[0].s1_density).toString(), 7))
        .line(NumberStr((this.results[0].s2_unit).toString(), 6) + NumberStr((this.results[0].s2_density).toString(), 7))
        .line(NumberStr((this.results[0].s3_unit).toString(), 6) + NumberStr((this.results[0].s3_density).toString(), 7))
        .line(NumberStr((this.results[0].g1_unit).toString(), 6) + NumberStr((this.results[0].g1_density).toString(), 7))
        .line(NumberStr((this.results[0].g2_unit).toString(), 6) + NumberStr((this.results[0].g2_density).toString(), 7))
        .line(NumberStr((this.results[0].g3_unit).toString(), 6) + NumberStr((this.results[0].g3_density).toString(), 7))
        .line(NumberStr((this.results[0].ad1_unit).toString(), 6) + NumberStr((this.results[0].ad1_density).toString(), 7))
        .line(NumberStr((this.results[0].ad2_unit).toString(), 6) + NumberStr((this.results[0].ad2_density).toString(), 7))
        .line(NumberStr((this.results[0].ad3_unit).toString(), 6) + NumberStr((this.results[0].ad3_density).toString(), 7))
        .line(NumberStr((this.results[0].air).toString(), 6)) // 목표공기량
        .line(NumberStr((this.results[0].aggregate).toString(), 6)) // 골재수정계수
        .line(NumberStr((this.results[0].wet).toString(), 6)) // 시멘트 습윤밀도
        .line(NumberStr((this.results[0].common_mass).toString(), 7)) // 용기질량
        .line(NumberStr((this.results[0].common_water).toString(), 7)) // 용기+물
        .line(NumberStr((this.results[0].common_volume).toString(), 7)) // 용기용적
        .line(NumberStr((this.results[0].input_slump).toString(), 7)) // 슬럼프
        .line(NumberStr((this.results[0].input_temp).toString(), 7)) // 온도
        .line(NumberStr((this.results[0].input_before).toString(), 7)) // 주수전질량
        .line(NumberStr((this.results[0].input_after).toString(), 7)) // 주수후질량
        .line(NumberStr((this.results[0].input_i_pressure).toString(), 7)) // 초기압력
        .line(NumberStr((this.results[0].input_e_pressure).toString(), 7)) // 평형압력
        .line(NumberStr(((this.results[0].mix_volume / 1000).toFixed(3)).toString(), 7)) // 이론용적
        .line(NumberStr((this.results[0].result_mass).toString(), 7)) // 단위용적질량
        .line(NumberStr((this.results[0].result_air).toString(), 7)) // 공기량
        .line(NumberStr((this.results[0].result_quantity).toString(), 7)) // 단위수량
        .end()
      }  
   }
   this.mountAlertBt(result.encode());

  }

  mountAlertBt(data) {
    let alert = this.alertCtrl.create({
      message: this.translate.instant('DETAILS.PRINT'),
      buttons: [{
        text: this.translate.instant('CANCEL'),
        role: 'cancel',
      },{
          text: this.translate.instant('CONFIRM'),
          role: 'submit',
          handler: (res) => {
            this.presentLoading();
            this.disconnect().then(() => {
              this.bluetooth.deviceConnection(this.printerUUID).then(success => {  
                this.isConnected = true;
                this.bluetooth.printForm(data)
                .then((_) => {
                  this.support.showAlert(this.translate.instant("SUCCESS_PRINT"));
                });
              }, fail => {
                this.isConnected = false;
                this.support.showAlert(this.translate.instant("PROBLEM"));
              });
            });
          }
        }]
    });
    alert.then(alert => alert.present());
  }

  disconnect() {
    this.ble.disconnect(this.peripheral.id);
    this.isConnected = false;
  }

  ngOnDestroy() {
    this.disconnect();
  }

  async updateMemo() {
    const alert = await this.alertCtrl.create({
      header: this.translate.instant('DETAILS.MEMO.UPDATE'),
      inputs: [
        {
          name: 'memo',
          value: this.results[0].memo,
          placeholder: this.translate.instant('DETAILS.MEMO.INPUT')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('CANCEL'),
          role: 'cancel'
        },
        {
          text: this.translate.instant('CONFIRM'),
          role: 'submit',
          handler: data => {
            this.postapi.updateMemo(this.resultId, {memo: data.memo}).subscribe( _ => {
              this.getapi.measureResult(this.resultId).subscribe((res) => {
                this.results[0].memo = res[0].memo;
                this.countData();
              });
              this.support.presentToast(this.translate.instant('UPDATE_COMPLETE'));
            });
          }
        }
      ],
    });
    await alert.present();
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: this.translate.instant('DELETE'),
      message: this.translate.instant('DETAILS.DELETE.QUESTION'),
      buttons: [
        {
          text: this.translate.instant('CANCEL'),
          role: 'cancel'
        },
        {
          text: this.translate.instant('CONFIRM'),
          role: 'submit',
          handler: _ => {
            this.postapi.deleteResult(this.resultId).subscribe((res: any) => {
              //this.router.navigate(['/app/complete-list']);
              this.location.back();
              this.support.presentToast(this.translate.instant('DETAILS.DELETE.DONE'));
            });
          }
        }
      ],
    });
    await alert.present();
  }

  countData() {
    const encoder = new EscPosEncoder();
    const escpos = encoder.initialize();

    if(this.isChecked) {  
      escpos
      .codepage('cp949')
      .line(`${this.results[0].cname}`)  //사업장
      .line(`${this.results[0].ct_name}`)  //계약자
      .line(`${this.results[0].sname}`)  //현장명
      .line(`${this.results[0].memo}`) //메모
      .line(`${(this.results[0].datetime).split(/[T.]+/)[0]} ${(this.results[0].datetime).split(/[T.]+/)[1]}`)  //날짜
      .line(`${this.results[0].mixnum}`) //배합번호
      .line(NumberStr((this.results[0].w1_unit).toString(), 6) + NumberStr((this.results[0].w1_density).toString(), 7))
      .line(NumberStr((this.results[0].w2_unit).toString(), 6) + NumberStr((this.results[0].w2_density).toString(), 7))
      .line(NumberStr((this.results[0].w3_unit).toString(), 6) + NumberStr((this.results[0].w3_density).toString(), 7))
      .line(NumberStr((this.results[0].c1_unit).toString(), 6) + NumberStr((this.results[0].c1_density).toString(), 7))
      .line(NumberStr((this.results[0].c2_unit).toString(), 6) + NumberStr((this.results[0].c2_density).toString(), 7))
      .line(NumberStr((this.results[0].c3_unit).toString(), 6) + NumberStr((this.results[0].c3_density).toString(), 7))
      .line(NumberStr((this.results[0].mad1_unit).toString(), 6) + NumberStr((this.results[0].mad1_density).toString(), 7))
      .line(NumberStr((this.results[0].mad2_unit).toString(), 6) + NumberStr((this.results[0].mad2_density).toString(), 7))
      .line(NumberStr((this.results[0].mad3_unit).toString(), 6) + NumberStr((this.results[0].mad3_density).toString(), 7))
      .line(NumberStr((this.results[0].s1_unit).toString(), 6) + NumberStr((this.results[0].s1_density).toString(), 7))
      .line(NumberStr((this.results[0].s2_unit).toString(), 6) + NumberStr((this.results[0].s2_density).toString(), 7))
      .line(NumberStr((this.results[0].s3_unit).toString(), 6) + NumberStr((this.results[0].s3_density).toString(), 7))
      .line(NumberStr((this.results[0].g1_unit).toString(), 6) + NumberStr((this.results[0].g1_density).toString(), 7))
      .line(NumberStr((this.results[0].g2_unit).toString(), 6) + NumberStr((this.results[0].g2_density).toString(), 7))
      .line(NumberStr((this.results[0].g3_unit).toString(), 6) + NumberStr((this.results[0].g3_density).toString(), 7))
      .line(NumberStr((this.results[0].ad1_unit).toString(), 6) + NumberStr((this.results[0].ad1_density).toString(), 7))
      .line(NumberStr((this.results[0].ad2_unit).toString(), 6) + NumberStr((this.results[0].ad2_density).toString(), 7))
      .line(NumberStr((this.results[0].ad3_unit).toString(), 6) + NumberStr((this.results[0].ad3_density).toString(), 7))
      .line(NumberStr((this.results[0].air).toString(), 6)) // 목표공기량
      .line(NumberStr((this.results[0].aggregate).toString(), 6)) // 골재수정계수
      .line(NumberStr((this.results[0].wet).toString(), 6)) // 시멘트 습윤밀도
      .line(NumberStr((this.results[0].common_mass).toString(), 7)) // 용기질량
      .line(NumberStr((this.results[0].common_water).toString(), 7)) // 용기+물
      .line(NumberStr((this.results[0].common_volume).toString(), 7)) // 용기용적
      .line(NumberStr((this.results[0].input_slump).toString(), 7)) // 슬럼프
      .line(NumberStr((this.results[0].input_temp).toString(), 7)) // 온도
      .line(NumberStr((this.results[0].input_before).toString(), 7)) // 주수전질량
      .line(NumberStr((this.results[0].input_after).toString(), 7)) // 주수후질량
      .line(NumberStr((this.results[0].input_i_pressure).toString(), 7)) // 초기압력
      .line(NumberStr((this.results[0].input_e_pressure).toString(), 7)) // 평형압력
      .line(NumberStr(((this.results[0].mix_volume / 1000).toFixed(3)).toString(), 7)) // 이론용적
      .line(NumberStr((this.results[0].result_mass).toString(), 7)) // 단위용적질량
      .line(NumberStr((this.results[0].result_air).toString(), 7)) // 공기량
      .line(NumberStr((this.results[0].result_quantity).toString(), 7)) // 단위수량
      
    } else {
      escpos
      .codepage('cp949')
      .line(`${this.results[0].cname}`)  //사업장
      .line(`${this.results[0].ct_name}`)  //계약자
      .line(`${this.results[0].sname}`)  //현장명
      .line(`${this.results[0].memo}`) //메모
      .line(`${(this.results[0].datetime).split(/[T.]+/)[0]}`)  //날짜
      .line(`${this.results[0].mixnum}`) //배합번호
      .line(NumberStr((this.results[0].w1_unit).toString(), 6) + NumberStr((this.results[0].w1_density).toString(), 7))
      .line(NumberStr((this.results[0].w2_unit).toString(), 6) + NumberStr((this.results[0].w2_density).toString(), 7))
      .line(NumberStr((this.results[0].w3_unit).toString(), 6) + NumberStr((this.results[0].w3_density).toString(), 7))
      .line(NumberStr((this.results[0].c1_unit).toString(), 6) + NumberStr((this.results[0].c1_density).toString(), 7))
      .line(NumberStr((this.results[0].c2_unit).toString(), 6) + NumberStr((this.results[0].c2_density).toString(), 7))
      .line(NumberStr((this.results[0].c3_unit).toString(), 6) + NumberStr((this.results[0].c3_density).toString(), 7))
      .line(NumberStr((this.results[0].mad1_unit).toString(), 6) + NumberStr((this.results[0].mad1_density).toString(), 7))
      .line(NumberStr((this.results[0].mad2_unit).toString(), 6) + NumberStr((this.results[0].mad2_density).toString(), 7))
      .line(NumberStr((this.results[0].mad3_unit).toString(), 6) + NumberStr((this.results[0].mad3_density).toString(), 7))
      .line(NumberStr((this.results[0].s1_unit).toString(), 6) + NumberStr((this.results[0].s1_density).toString(), 7))
      .line(NumberStr((this.results[0].s2_unit).toString(), 6) + NumberStr((this.results[0].s2_density).toString(), 7))
      .line(NumberStr((this.results[0].s3_unit).toString(), 6) + NumberStr((this.results[0].s3_density).toString(), 7))
      .line(NumberStr((this.results[0].g1_unit).toString(), 6) + NumberStr((this.results[0].g1_density).toString(), 7))
      .line(NumberStr((this.results[0].g2_unit).toString(), 6) + NumberStr((this.results[0].g2_density).toString(), 7))
      .line(NumberStr((this.results[0].g3_unit).toString(), 6) + NumberStr((this.results[0].g3_density).toString(), 7))
      .line(NumberStr((this.results[0].ad1_unit).toString(), 6) + NumberStr((this.results[0].ad1_density).toString(), 7))
      .line(NumberStr((this.results[0].ad2_unit).toString(), 6) + NumberStr((this.results[0].ad2_density).toString(), 7))
      .line(NumberStr((this.results[0].ad3_unit).toString(), 6) + NumberStr((this.results[0].ad3_density).toString(), 7))
      .line(NumberStr((this.results[0].air).toString(), 6)) // 목표공기량
      .line(NumberStr((this.results[0].aggregate).toString(), 6)) // 골재수정계수
      .line(NumberStr((this.results[0].wet).toString(), 6)) // 시멘트 습윤밀도
      .line(NumberStr((this.results[0].common_mass).toString(), 7)) // 용기질량
      .line(NumberStr((this.results[0].common_water).toString(), 7)) // 용기+물
      .line(NumberStr((this.results[0].common_volume).toString(), 7)) // 용기용적
      .line(NumberStr((this.results[0].input_slump).toString(), 7)) // 슬럼프
      .line(NumberStr((this.results[0].input_temp).toString(), 7)) // 온도
      .line(NumberStr((this.results[0].input_before).toString(), 7)) // 주수전질량
      .line(NumberStr((this.results[0].input_after).toString(), 7)) // 주수후질량
      .line(NumberStr((this.results[0].input_i_pressure).toString(), 7)) // 초기압력
      .line(NumberStr((this.results[0].input_e_pressure).toString(), 7)) // 평형압력
      .line(NumberStr(((this.results[0].mix_volume / 1000).toFixed(3)).toString(), 7)) // 이론용적
      .line(NumberStr((this.results[0].result_mass).toString(), 7)) // 단위용적질량
      .line(NumberStr((this.results[0].result_air).toString(), 7)) // 공기량
      .line(NumberStr((this.results[0].result_quantity).toString(), 7)) // 단위수량
      
    }
    this.DS = (escpos.encode()).length - 5;
  }


  loadStoredImages() {
    this.storage.get(STORAGE_KEY).then(images => {
      if (images) {
        let arr = JSON.parse(images);
        this.images = [];
        for (let img of arr) {
          let filePath = this.file.dataDirectory + img;
          let resPath = this.pathForImage(filePath);
          this.images.push({ name: img, path: resPath, filePath: filePath });
        }
      }
    });
  }
 
  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      let converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
        header: "이미지 소스 선택",
        buttons: [{
                text: '갤러리 업로드',
                handler: () => {
                    this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
                }
            },
            {
                text: '카메라',
                handler: () => {
                    this.takePicture(this.camera.PictureSourceType.CAMERA);
                }
            },
            {
                text: '취소',
                role: 'cancel'
            }
        ]
    });
    await actionSheet.present();
  }
  
  takePicture(sourceType: PictureSourceType) {
      var options: CameraOptions = {
          quality: 100,
          sourceType: sourceType,
          saveToPhotoAlbum: false,
          correctOrientation: true
      };
  
      this.camera.getPicture(options).then(imagePath => {
          if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
              this.filePath.resolveNativePath(imagePath)
                  .then(filePath => {
                      let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                      let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                  });
          } else {
              var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
              var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
              this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          }
      });
  }

  createFileName() {
    var d = new Date(),
        n = d.getTime(),
        newFileName = n + ".jpg";
    return newFileName;
  }
 
  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
        this.updateStoredImages(newFileName);
    }, error => {
        this.support.presentToast('Error while storing file.');
    });
  }
 
  updateStoredImages(name) {
    this.storage.get(STORAGE_KEY).then(images => {
        let arr = JSON.parse(images);
        if (!arr) {
            let newImages = [name];
            this.storage.set(STORAGE_KEY, JSON.stringify(newImages));
        } else {
            arr.push(name);
            this.storage.set(STORAGE_KEY, JSON.stringify(arr));
        }
 
        let filePath = this.file.dataDirectory + name;
        let resPath = this.pathForImage(filePath);
 
        let newEntry = {
            name: name,
            path: resPath,
            filePath: filePath
        };
 
        this.images = [newEntry, ...this.images];
        this.ref.detectChanges(); // trigger change detection cycle
    });
  }

  deleteImage(imgEntry, position) {
    this.images.splice(position, 1);
 
    this.storage.get(STORAGE_KEY).then(images => {
        let arr = JSON.parse(images);
        let filtered = arr.filter(name => name != imgEntry.name);
        this.storage.set(STORAGE_KEY, JSON.stringify(filtered));
 
        var correctPath = imgEntry.filePath.substr(0, imgEntry.filePath.lastIndexOf('/') + 1);
 
        this.file.removeFile(correctPath, imgEntry.name).then(res => {
            this.support.presentToast('파일이 삭제되었습니다.');
        });
    });
  }

  startUpload(imgEntry) {
    this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
        .then(entry => {
            ( < FileEntry > entry).file(file => this.readFile(file))
        })
        .catch(err => {
            this.support.presentToast('Error while reading file.');
        });
  }
 
  readFile(file: any) {
    const reader = new FileReader();
    reader.onload = () => {
        const formData = new FormData();
        const imgBlob = new Blob([reader.result], {
            type: file.type
        });
        formData.append('file', imgBlob, file.name);
        formData.append('id', this.resultId);
        this.uploadImageData(formData);
    };
    reader.readAsArrayBuffer(file);
  }

  async uploadImageData(formData: FormData) {
    const loading = await this.loadingController.create({
        message: '이미지 업로드 중...',
    });
    await loading.present();

    this.http.put("http://app.dymeter.com:4000/api/upload-image", formData)
    .pipe(
        finalize(() => {
            loading.dismiss();
        })
    )
    .subscribe(_ => {
      this.getapi.measureResult(this.resultId).subscribe((res) => {
        this.displayedImage = res[0].imgurl;
      });
      this.support.presentToast('파일이 업로드 되었습니다.')
    });
  }

  doRefresh(event) {
    this.getapi.measureResult(this.resultId).subscribe((res) => {
      this.displayedImage = res[0].imgurl;
    });
    setTimeout(() => {
      event.target.complete();
    }, 1000);
  }
}

function NumberStr(tempstr: string, n: number) {
  let temp = new Array(), k=0;
  const i = tempstr.length;

  if (i > n) {
    console.log('Overflow Digit');
    return;
  }
  if(n == 6) {
    temp = [-1,-1,-1,-1,-1,-1];
  } else {
    temp = [-1,-1,-1,-1,-1,-1,-1];
  }

  for(let j=n-i; j < n; j++) {
    temp[j] = tempstr.charAt(k);
    k++;
  }
  let str = temp.join('');
  return str.replace(/-1/g,' ');
}
