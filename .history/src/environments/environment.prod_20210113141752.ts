export const environment = {
  production: true,

  url: 'http://app.dymeter.com:4001/api',
  jwt_token: 'jwt_token',
  user_id: 'user_id',
  device_id: 'device_id',

  locker: '1234',

};
